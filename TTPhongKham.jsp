<%-- 
    Document   : TTPhongKham
    Created on : May 21, 2023, 10:20:19 AM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>An Tâm</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/gijgo.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/animated-headline.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <!-- ? Preloader Start -->
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="preloader-circle"></div>
                    <div class="preloader-img pere-text">
                        <img src="assets/img/logo/logo_3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- Preloader Start -->
        <header>
            <!--? Header Start -->
            <div class="header-area">
                <div class="main-header header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2 col-md-1">
                                <div class="logo">
                                    <a href="trang-chu"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                </div>
                                
                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-10">
                                <div class="menu-main d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu f-right d-none d-lg-block">
                                        <nav> 
                                            <ul id="navigation">
                                                <li><a href="trang-chu">Trang chủ</a></li>
                                                <li><a>Thông tin</a>
                                                    <ul class="submenu">
                                                        <li><a href="TTPhongKham.jsp">Phòng Khám</a></li>
                                                        <li><a href="bac-si">Bác Sĩ</a></li>

                                                    </ul>
                                                </li>

                                                <li><a href="cam-nang">Cẩm Nang</a>
                                                    
                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header-right-btn f-right d-none d-lg-block ml-15">
                                        <a href="#" class="btn header-btn">Đăng nhập/Đăng kí</a>
                                    </div>
                                </div>
                            </div>  
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header End -->
        </header>
        <main>
            <!--? Slider Area Start-->
            <div class="slider-area slider-area2">
                <div class="slider-active dot-style">
                    <!-- Slider Single -->
                    <div class="single-slider  d-flex align-items-center slider-height2">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-8 col-md-10 ">
                                    <div class="hero-wrapper">
                                        <div class="hero__caption">
                                            <h1 data-animation="fadeInUp" data-delay=".3s" style="font-family: Arial">Phòng Khám</h1>
                                            <p data-animation="fadeInUp" data-delay=".6s"style="font-family: Arial">An Tâm - Nơi bạn tìm thấy sự yên tâm
                                                <br style="font-family: Arial"> cho từng tế bào.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <!-- Slider Area End -->
            <!--? Team Area Start-->
            <section class="team-area pb-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-cat text-center mb-30">
                                <div class="cat-icon">
                                    <img src="assets/img/gallery/team1.png" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h5><a href="#" style="font-family: Arial">Tận tâm</a></h5>
                                    <p style="font-family: Arial; text-align: justify;">Phòng khám luôn tận tâm và chu đáo đối với mỗi bệnh nhân. 
                                        Từ việc lắng nghe tình trạng sức khỏe chi tiết của bệnh nhân đến việc cung cấp thông tin và giải đáp thắc mắc, 
                                        mọi người đều tạo ra một môi trường chăm sóc tận tâm và gần gũi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-cat text-center mb-30">
                                <div class="cat-icon">
                                    <img src="assets/img/gallery/team2.png" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h5><a href="#"style="font-family: Arial">Đội ngũ chuyên gia</a></h5>
                                    <p style="font-family: Arial; text-align: justify;">Phòng khám có một đội ngũ y bác sĩ và nhân viên y tế có trình độ chuyên môn cao và giàu kinh nghiệm. 
                                        Họ là những chuyên gia được đào tạo chuyên sâu trong các lĩnh vực khác nhau, đảm bảo rằng bệnh nhân sẽ nhận 
                                        được sự chăm sóc tốt nhất.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-cat text-center mb-30">
                                <div class="cat-icon">
                                    <img src="assets/img/gallery/team3.png" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h5><a href="#" style="font-family: Arial;" >Cơ sở vật chất hiện đại</a></h5>
                                    <p style="font-family: Arial; text-align: justify;" >Phòng khám được trang bị các trang thiết bị y tế tiên tiến và công nghệ mới nhất để đảm bảo chẩn đoán chính xác 
                                        và điều trị hiệu quả. Môi trường phòng khám thoáng đãng, sạch sẽ và tạo cảm giác thoải mái cho bệnh nhân.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--? Team End-->
            <!-- Services End-->
            <!--? About-2 Area Start -->
            <div class="about-area2 section-padding40">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-12">
                            <!-- about-img -->
                            <div class="about-img ">
                                <img src="assets/img/gallery/about.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <div class="about-caption mb-50">
                                <!-- Section Tittle -->
                                <div class="section-tittle mb-35">
                                    <h2 style="font-family: Arial">Chúng tôi lo lắng cho bạn!</h2>
                                </div>
                                <p class="pera-top mb-40" style="font-family: Arial; text-align: justify">Yêu thương và chăm sóc là một bước đi, nhưng cũng là một điều tất yếu trong phòng khám chúng tôi.</p>
                                <p style="font-family: Arial; text-align: justify"> Nhiệt huyết đam mê, phòng khám An Tâm đã xây dựng một tầm vóc vững chắc trong lĩnh vực khám bệnh. 
                                    Với sự chuyên nghiệp và tận tâm, chúng tôi luôn cam kết đem đến cho bệnh nhân những trải nghiệm y tế tốt nhất. 
                                    Đội ngũ y bác sĩ giàu kinh nghiệm cùng với cơ sở vật chất hiện đại, chúng tôi đảm bảo mọi quy trình khám và điều trị diễn 
                                    ra một cách an toàn và hiệu quả. Hãy đến với An Tâm để cảm nhận sự chăm sóc chuyên nghiệp và sự yên tâm tuyệt đối trong mỗi 
                                    cuộc hẹn y tế.</p>
                                <div class="icon-about">
                                    <img src="assets/img/icon/about1.svg" alt="" class=" mr-20">
                                    <img src="assets/img/icon/about2.svg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--? Testimonial Area Start -->

            <!--? Testimonial Area End -->
            <!--? video_start -->
            <div class="container">
                <div class="video-area section-bg2 d-flex align-items-center"  data-background="assets/img/gallery/video-bg.png">
                    <div class="video-wrap position-relative">
                        <div class="video-icon" >
                            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=FcZTDoueylQ"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- video_end -->      

            <!--? About Law Start-->
            <section class="about-low-area mt-30">
                <div class="container">
                    <div class="about-cap-wrapper">
                        <div class="row">
                            <div class="col-xl-5  col-lg-6 col-md-10 offset-xl-1">
                                <div class="about-caption mb-50">
                                    <!-- Section Tittle -->
                                    <div class="section-tittle mb-35">
                                        <h2 style="font-family: Arial;"> 100% Mang đến sự hài lòng cho bạn.</h2>
                                    </div>
                                    <p style="font-family: Arial;" >Hãy đến với chúng tôi. Phòng khám An Tâm - Yên tâm trong từng tế bào.</p>
                                    <a href="about.html" class="border-btn">Đặt lịch ngay!</a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <!-- about-img -->
                                <div class="about-img">
                                    <div class="about-font-img">
                                        <img src="assets/img/gallery/about2.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About Law End-->
        </main>
        <footer>
            <div class="footer-wrappr section-bg3" data-background="assets/img/gallery/footer-bg.png">
                <div class="footer-area footer-padding ">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                <div class="single-footer-caption mb-50">
                                    <!-- logo -->
                                    <div class="footer-logo mb-25">
                                        <a href="index.html"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                    </div>
                                    <d iv class="header-area">
                                        <div class="main-header main-header2">
                                            <div class="menu-main d-flex align-items-center justify-content-start">
                                                <!-- Main-menu -->

                                            </div>  
                                        </div>
                                    </d>


                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="single-footer-caption">
                                    <div class="footer-tittle mb-50">
                                        <h4>Subscribe newsletter</h4>
                                    </div>
                                    <!-- Form -->
                                    <div class="footer-form">
                                        <div id="mc_embed_signup">
                                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative mail_part" novalidate="true">
                                                <input type="email" name="EMAIL" id="newsletter-form-email" placeholder=" Email Address " class="placeholder hide-on-focus" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'">
                                                <div class="form-icon">
                                                    <button type="submit" name="submit" id="newsletter-submit" class="email_icon newsletter-submit button-contactForm">
                                                        Subscribe
                                                    </button>
                                                </div>
                                                <div class="mt-10 info"></div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-bottom area -->

            </div>
        </footer>
        <!-- Scroll Up -->
        <div id="back-top" >
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div>

        <!-- JS here -->

        <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
        <!-- Jquery, Popper, Bootstrap -->
        <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap.min.js"></script>
        <!-- Jquery Mobile Menu -->
        <script src="./assets/js/jquery.slicknav.min.js"></script>

        <!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="./assets/js/owl.carousel.min.js"></script>
        <script src="./assets/js/slick.min.js"></script>
        <!-- One Page, Animated-HeadLin -->
        <script src="./assets/js/wow.min.js"></script>
        <script src="./assets/js/animated.headline.js"></script>
        <script src="./assets/js/jquery.magnific-popup.js"></script>

        <!-- Date Picker -->
        <script src="./assets/js/gijgo.min.js"></script>
        <!-- Nice-select, sticky -->
        <script src="./assets/js/jquery.nice-select.min.js"></script>
        <script src="./assets/js/jquery.sticky.js"></script>

        <!-- counter , waypoint,Hover Direction -->
        <script src="./assets/js/jquery.counterup.min.js"></script>
        <script src="./assets/js/waypoints.min.js"></script>
        <script src="./assets/js/jquery.countdown.min.js"></script>
        <script src="./assets/js/hover-direction-snake.min.js"></script>

        <!-- contact js -->
        <script src="./assets/js/contact.js"></script>
        <script src="./assets/js/jquery.form.js"></script>
        <script src="./assets/js/jquery.validate.min.js"></script>
        <script src="./assets/js/mail-script.js"></script>
        <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

        <!-- Jquery Plugins, main Jquery -->	
        <script src="./assets/js/plugins.js"></script>
        <script src="./assets/js/main.js"></script>

    </body>
</html>