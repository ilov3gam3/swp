<%-- 
    Document   : Trangchu
    Created on : May 21, 2023, 7:17:18 PM
    Author     : ASUS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>An Tâm</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/gijgo.css">
        <link rel="stylesheet" href="assets/css/search.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/animated-headline.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <!-- ? Preloader Start -->
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="preloader-circle"></div>
                    <div class="preloader-img pere-text">
                        <img src="assets/img/logo/loder.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- Preloader Start -->
        <header>
            <!--? Header Start -->
            <div class="header-area">
                <div class="main-header header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2 col-md-1">
                                <div class="logo">
                                    <a href="index.html"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                </div>

                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-10">
                                <div class="menu-main d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu f-right d-none d-lg-block">
                                        <nav> 
                                            <ul id="navigation">
                                                <li><a href="trang-chu">Trang chủ</a></li>
                                                <li><a>Thông tin</a>
                                                    <ul class="submenu">
                                                        <li><a href="TTPhongKham.jsp">Phòng Khám</a></li>
                                                        <li><a href="bac-si">Bác Sĩ</a></li>

                                                    </ul>
                                                </li>

                                                <li><a href="blog.html">Cẩm Nang</a>

                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header-right-btn f-right d-none d-lg-block ml-15">
                                        <a href="#" class="btn header-btn">Đăng nhập/Đăng kí</a>
                                    </div>
                                </div>
                            </div>  
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header End -->
        </header>
        <main>
            <!--? Slider Area Start-->
            <div class="slider-area">
                <div class="slider-active dot-style">
                    <!-- Slider Single -->
                    <div class="single-slider d-flex align-items-center slider-height">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-8 col-md-10 ">
                                    <div class="hero-wrapper">
                                        <!-- Video icon -->
                                        <div class="video-icon">
                                            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0" data-animation="bounceIn" data-delay=".4s">
                                                <i class="fas fa-play"></i>
                                            </a>
                                        </div>
                                        <div class="hero__caption">
                                            <h1 style="font-family: Arial" data-animation="fadeInUp" data-delay=".3s">"Tài Sản Đầu Tiên Là Sức Khỏe"</h1>
                                            <p style="font-family: Arial" data-animation="fadeInUp" data-delay=".6s">Người có sức khỏe, có hy vọng;<br> người có hy vọng, có tất cả mọi thứ.</p>
                                            <a href="services.html" class="btn" data-animation="fadeInLeft" data-delay=".3s">Đặt Khám</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <!-- Slider Single -->
                    <div class="single-slider d-flex align-items-center slider-height">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-8 col-md-10 ">
                                    <div class="hero-wrapper">
                                        <!-- Video icon -->
                                        <div class="video-icon">
                                            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0" data-animation="bounceIn" data-delay=".4s">
                                                <i class="fas fa-play"></i>
                                            </a>
                                        </div>
                                        <div class="hero__caption">
                                            <h1 style="font-family: Arial" data-animation="fadeInUp" data-delay=".3s">"Tài Sản Đầu Tiên Là Sức Khỏe"</h1>
                                            <p style="font-family: Arial" data-animation="fadeInUp" data-delay=".6s">Người có sức khỏe, có hy vọng;<br> người có hy vọng, có tất cả mọi thứ.</p>
                                            <a style="font-family: Arial" href="#" class="btn" data-animation="fadeInLeft" data-delay=".3s">Đặt Khám</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <!-- Slider Area End -->


            <!--? Blog Area Start -->
            <section class="home-blog-area section-padding30">
                <div class="container">
                    <!-- Section Tittle -->
                    <div class="row justify-content-center">
                        <div class="col-lg-7 col-md-9 col-sm-10">
                            <div class="section-tittle text-center mb-100">
                                <h2 style="font-family: Arial; color: #009900;">Bác Sĩ Nổi Bật</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <c:forEach items="${showtop3}" var="i">
                            <div class="col-lg-4 col-md-6">
                                <div class="home-blog-single mb-40">
                                    <div class="blog-img-cap">
                                        <div class="blog-img">
                                            <img src="${i.image}" alt="">
                                        </div>
                                        <div class="blog-cap">
                                            <ul>
                                                <li><h3 class="fa fa-user-md">  ${i.doctorName}</h3></li> <br>
                                                <li><h3 class="fas fa-graduation-cap">${i.degree}</h3></li> <br>
                                           
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>

                    </div>
                </div>
            </section>
            <!-- Blog Area End -->
            <!--? About Law Start-->
            <section class="about-low-area mt-30">
                <div class="container">
                    <div class="about-cap-wrapper">
                        <div class="row">
                            <div class="col-xl-5  col-lg-6 col-md-10 offset-xl-1">
                                <div class="about-caption mb-50">
                                    <!-- Section Tittle -->
                                    <div class="section-tittle mb-35">
                                        <br><br>
                                        <h2 style="font-family: Arial">100% Mang đến sự hài lòng cho bạn.</h2>
                                    </div>
                                    <p style="font-family: Arial;" >Hãy đến với chúng tôi. Phòng khám An Tâm - Yên tâm trong từng tế bào.</p>
                                    <a href="about.html" class="border-btn">Đặt lịch ngay!</a>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <!-- about-img -->
                                <div class="about-img">
                                    <div class="about-font-img">
                                        <img src="assets/img/gallery/about2.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About Law End-->
        </main>
        <footer>
            <div class="footer-wrappr section-bg3" data-background="assets/img/gallery/footer-bg.png">
                <div class="footer-area footer-padding ">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                <div class="single-footer-caption mb-50">
                                    <!-- logo -->
                                    <div class="footer-logo mb-25">
                                        <a href="index.html"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                    </div>
                                    <div class="header-area">
                                        <div class="main-header main-header2 d-flex align-items-center justify-content-between">
                                            <!-- social -->

                                            <!-- Main-menu -->
 
                                        </div>
                                    </div>
                                </div>
                                <!-- Contact Information -->
                                <div class="contact-info">
                                    <ul>
                                        <li><h3 class="fas fa-map-marker-alt"> 123 Ngũ Hành Sơn, TP Đà Nẵng</h3></li> <br>
                                        <li><h3 class="fas fa-phone"> +84 1900 8369</h3></li> <br>
                                        <li><h3 class="far fa-envelope"> antam@gmail.com</h3></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </footer>
        <!-- Scroll Up -->
        <div id="back-top" >
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div>

        <!-- JS here -->

        <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
        <!-- Jquery, Popper, Bootstrap -->
        <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap.min.js"></script>
        <!-- Jquery Mobile Menu -->
        <script src="./assets/js/jquery.slicknav.min.js"></script>

        <!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="./assets/js/owl.carousel.min.js"></script>
        <script src="./assets/js/slick.min.js"></script>
        <!-- One Page, Animated-HeadLin -->
        <script src="./assets/js/wow.min.js"></script>
        <script src="./assets/js/animated.headline.js"></script>
        <script src="./assets/js/jquery.magnific-popup.js"></script>

        <!-- Date Picker -->
        <script src="./assets/js/gijgo.min.js"></script>
        <!-- Nice-select, sticky -->
        <script src="./assets/js/jquery.nice-select.min.js"></script>
        <script src="./assets/js/jquery.sticky.js"></script>

        <!-- counter , waypoint,Hover Direction -->
        <script src="./assets/js/jquery.counterup.min.js"></script>
        <script src="./assets/js/waypoints.min.js"></script>
        <script src="./assets/js/jquery.countdown.min.js"></script>
        <script src="./assets/js/hover-direction-snake.min.js"></script>

        <!-- contact js -->
        <script src="./assets/js/contact.js"></script>
        <script src="./assets/js/jquery.form.js"></script>
        <script src="./assets/js/jquery.validate.min.js"></script>
        <script src="./assets/js/mail-script.js"></script>
        <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

        <!-- Jquery Plugins, main Jquery -->	
        <script src="./assets/js/plugins.js"></script>
        <script src="./assets/js/main.js"></script>

    </body>
</html>