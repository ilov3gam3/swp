<%-- 
    Document   : Trangchu
    Created on : May 21, 2023, 7:17:18 PM
    Author     : ASUS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>An Tâm</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/gijgo.css">
        <link rel="stylesheet" href="assets/css/search.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/animated-headline.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/cssformdoctor.css">

    </head>
    <body>
        <!-- ? Preloader Start -->
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="preloader-circle"></div>
                    <div class="preloader-img pere-text">
                        <img src="assets/img/logo/logo_3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- Preloader Start -->
        <header>
            <!--? Header Start -->
            <div class="header-area">
                <div class="main-header header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2 col-md-1">
                                <div class="logo">
                                    <a href="trang-chu"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                </div>

                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-10">
                                <div class="menu-main d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu f-right d-none d-lg-block">
                                        <nav> 
                                            <ul id="navigation">
                                                <li><a href="trang-chu">Trang chủ</a></li>
                                                <li><a>Thông tin</a>
                                                    <ul class="submenu">
                                                        <li><a href="TTPhongKham.jsp">Phòng Khám</a></li>
                                                        <li><a href="bac-si">Bác Sĩ </a></li>

                                                    </ul>
                                                </li>

                                                <li><a href="cam-nang">Cẩm Nang</a>

                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header-right-btn f-right d-none d-lg-block ml-15">
                                        <c:if test="${sessionScope.acc == null}">

                                            <a href="Login.jsp" class="btn header-btn">Đăng nhập/Đăng kí</a>
                                        </c:if>
                                    </div> 
                                     <div class="main-menu f-right d-none d-lg-block">
                                    <c:if test="${sessionScope.acc != null}">
                                        <ul id="navigation">
                                           
                                            <li><a > <i class="fa fa-user" aria-hidden="true"> Hello ${sessionScope.acc.name} </i> </a>                                         
                                                <ul class="submenu">
                                                    <li><a href="dang-xuat">Đăng xuất</a></li>
                                                   
                                                    <li><a href="chinh-sua?=${sessionScope.acc.id}">Trang cá nhân </a></li>
                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                    </c:if>
                                     </div>

                                </div>
                            </div>
                        </div>  
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Header End -->
        </header>
        <main>
            <!--? Slider Area Start-->
            <div class="slider-area">
                <div class="slider-active dot-style">
                    <!-- Slider Single -->
                    <div class="single-slider d-flex align-items-center slider-height">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-8 col-md-10 ">
                                    <div class="hero-wrapper">
                                        <!-- Video icon -->
                                        <div class="video-icon">
                                            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0" data-animation="bounceIn" data-delay=".4s">
                                                <i class="fas fa-play"></i>
                                            </a>
                                        </div>
                                        <div class="hero__caption">
                                            <h1 style="font-family: Arial" data-animation="fadeInUp" data-delay=".3s">"Tài Sản Đầu Tiên Là Sức Khỏe"</h1>
                                            <p style="font-family: Arial" data-animation="fadeInUp" data-delay=".6s">Người có sức khỏe, có hy vọng;<br> người có hy vọng, có tất cả mọi thứ.</p>
                                            <a href="services.html" class="btn" data-animation="fadeInLeft" data-delay=".3s">Đặt Khám</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <!-- Slider Single -->
                    <div class="single-slider d-flex align-items-center slider-height">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-8 col-md-10 ">
                                    <div class="hero-wrapper">
                                        <!-- Video icon -->
                                        <div class="video-icon">
                                            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0" data-animation="bounceIn" data-delay=".4s">
                                                <i class="fas fa-play"></i>
                                            </a>
                                        </div>
                                        <div class="hero__caption">
                                            <h1 style="font-family: Arial" data-animation="fadeInUp" data-delay=".3s">"Tài Sản Đầu Tiên Là Sức Khỏe"</h1>
                                            <p style="font-family: Arial" data-animation="fadeInUp" data-delay=".6s">Người có sức khỏe, có hy vọng;<br> người có hy vọng, có tất cả mọi thứ.</p>
                                            <a style="font-family: Arial" href="#" class="btn" data-animation="fadeInLeft" data-delay=".3s">Đặt Khám</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <!-- Slider Area End -->
            <!--? About-2 Area Start -->
            <div class="about-area2 section-padding40">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-12">
                            <!-- about-img -->
                            <div class="about-img ">
                                <img src="assets/img/gallery/about.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <div class="about-caption">
                                <!-- Section Tittle -->
                                <div class="section-tittle mb-35">
                                    <h2 style="font-family: Arial">Vẻ đẹp bên ngoài 
                                        bắt nguồn từ sức khỏe bên trong!</h2>
                                </div>
                                <p style="font-family: Arial" class="pera-bottom mb-30">Sức khỏe là thứ mà chúng ta không nhìn thấy được,
                                    là yếu tố sống còn của mỗi con người. Hãy nâng niu quý trọng sức khỏe,
                                    đừng để khi mất rồi mới thấy hối tiếc.</p>
                                <div class="icon-about">
                                    <img src="assets/img/icon/about1.svg" alt="" class=" mr-20">
                                    <img src="assets/img/icon/about2.svg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About-2 Area End -->
            <section class="wantToWork-area section-bg3" data-background="assets/img/gallery/section_bg01.png">
                <div class="container">
                    <div class="wants-wrapper w-padding2">
                        <div class="row align-items-center justify-content-between">
                            <div class="col-xl-7 col-lg-9 col-md-8">
                                <div class="wantToWork-caption wantToWork-caption2">
                                    <h2 style="font-family: Arial">Cẩm Nang Y Tế</h2>
                                    <h3 style="font-family: Arial" >Bí quyết chắm sóc sức khỏe và thấu hiểu cơ thể.</h3>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4">
                                <a style="font-family: Arial" href="services.html" class="btn f-right sm-left">Đặt Khám</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--? Services Area Start -->
            <div class="service-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat text-center mb-50">
                                <div class="cat-icon">
                                    <img src="assets/img/icon/services1.svg" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h1><a style="font-family: Arial" href="services.html">Khi nào chúng ta cần đi khám <br> sức khỏe tim mạch?</a></h1>
                                    <p style="font-family: Arial; text-align: justify;" >Hệ tuần hoàn là hệ thống cấp máu nuôi dưỡng toàn cơ thể. 
                                        Để đảm bảo cơ thể luôn khỏe mạnh, các bác sĩ vẫn khuyên bạn đi khám tim mạch định kỳ hoặc ngay khi có dấu hiệu 
                                        gợi ý bệnh tim mạch. </p>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat text-center mb-50">
                                <div class="cat-icon">
                                    <img src="assets/img/icon/services2.svg" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h1><a style="font-family: Arial" href="services.html">Bệnh viêm phổi <br> Nguyên nhân, triệu chứng, phân loại</a></h1>
                                    <p style="font-family: Arial; text-align: justify;">Bệnh viêm phổi được biết đến như là bệnh lý nhiễm trùng đường hô hấp dưới gặp phổ biến nhất. 
                                        Bệnh gặp ở mọi lứa tuổi và con số tử vong hàng năm do bệnh viêm phổi lên đến hàng triệu người trên thế giới.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat text-center mb-50">
                                <div class="cat-icon">
                                    <img src="assets/img/icon/services3.svg" alt="">
                                </div>
                                <div class="cat-cap">
                                    <h1><a style="font-family: Arial" href="services.html">10 nguyên nhân <br> đau xương khớp ở người cao tuổi</a></h1>
                                    <p style="font-family: Arial; text-align: justify;">Bệnh xương khớp ở người cao tuổi là bệnh của hệ thống cơ, xương và khớp, 
                                        thường được biểu hiện bằng các triệu chứng đau, sưng khớp, hạn chế vận động, yếu cơ, biến dạng xương… </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Services Area End -->
            <!--? Testimonial Area Start -->
            <section class="testimonial-area testimonial-padding fix">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class=" col-lg-9">
                            <div class="about-caption">
                                <!-- Testimonial Start -->
                                <div class="h1-testimonial-active dot-style">
                                    <!-- Single Testimonial -->
                                    <div class="single-testimonial position-relative">
                                        <div class="testimonial-caption">
                                            <img src="assets/img/icon/quotes-sign.png" alt="" class="quotes-sign">
                                            <p style="font-family: Arial">"Người ta không coi trọng sức khỏe cho đến khi...<br> ...đau yếu"</p>
                                        </div>
                                        <!-- founder -->
                                        <div class="testimonial-founder d-flex align-items-center">
                                            <div class="founder-img">
                                                <img src="assets/img/icon/testimonial.png" alt="">
                                            </div>
                                            <div class="founder-text">
                                                <span>Thomas Fuller</span>
                                                <p>(1608 - 1661) Giáo sĩ và nhà sử học người Anh.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Testimonial -->
                                    <div class="single-testimonial position-relative">
                                        <div class="testimonial-caption">
                                            <img src="assets/img/icon/quotes-sign.png" alt="" class="quotes-sign">
                                            <p style="font-family: Arial">"Chăm sóc sức khỏe ngày hôm nay cho tôi hy vọng tươi sáng hơn vào ngày mai" </p>
                                        </div>
                                        <!-- founder -->
                                        <div class="testimonial-founder d-flex align-items-center">
                                            <div class="founder-img">
                                                <img src="assets/img/icon/testimonial.png" alt="">
                                            </div>
                                            <div class="founder-text">
                                                <span>Anne Wilson Schaef</span>
                                                <p>Nhà tâm lý, nhà hoạt động nữ quyền người Mỹ, đã xây dựng cách tiếp cận riêng của mình trong việc chữa lành tâm hồn con người, 
                                                    một phương thức mà bà gọi là Sống không ngừng nghỉ (Living in Process).</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Testimonial End -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--? Testimonial Area End -->

            <!--? Blog Area Start -->
            <section class="home-blog-area section-padding30">
                <div class="container">
                    <!-- Section Tittle -->
                    <div class="row justify-content-center">
                        <div class="col-lg-7 col-md-9 col-sm-10">
                            <div class="section-tittle text-center mb-100">
                                <h2 style="font-family: Arial;">Bác Sĩ Ưu Tú</h2>








                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <c:forEach items="${showtop3}" var="i">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="doctor-card">
                                    <div class="blog-img-cap">
                                        <div class="blog-img">
                                            <img src="${i.image}" alt="">
                                            <div class="ratings">
                                                <span class="rating"></span>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <div class="blog-cap">
                                            <h3 class="doctor-name">1</h3>
                                            <div class="degree">${i.degree}</div>



                                            <li>
                                                <button class="btn appointment-button">
                                                    <i class="fas fa-calendar-alt" style="color: white;"></i>
                                                    <span>Đặt khám</span>
                                                </button>
                                            </li>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>

                    </div>

                </div>
                </div>
            </section>
            <!-- Blog Area End -->
            <!--? About Law Start-->
            <section class="about-low-area mt-30">
                <div class="container">
                    <div class="about-cap-wrapper">
                        <div class="row">
                            <div class="col-xl-5  col-lg-6 col-md-10 offset-xl-1">
                                <div class="about-caption mb-50">
                                    <!-- Section Tittle -->
                                    <div class="section-tittle mb-35">
                                        <br><br>
                                        <h2 style="font-family: Arial">100% Mang đến sự hài lòng cho bạn.</h2>
                                    </div>
                                    <p style="font-family: Arial;" >Hãy đến với chúng tôi. Phòng khám An Tâm - Yên tâm trong từng tế bào.</p>
                                    <a href="about.html" class="border-btn">Đặt lịch ngay!</a>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <!-- about-img -->
                                <div class="about-img">
                                    <div class="about-font-img">
                                        <img src="assets/img/gallery/about2.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About Law End-->
        </main>
        <footer>
            <div class="footer-wrappr section-bg3" data-background="assets/img/gallery/footer-bg.png">
                <div class="footer-area footer-padding ">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-8 col-lg-8 col-md-6 col-sm-12">
                                <div class="single-footer-caption mb-50">
                                    <!-- logo -->
                                    <div class="footer-logo mb-25">
                                        <a href="index.html"><img src="assets/img/logo/logo_5.png" alt=""></a>
                                    </div>
                                    <div class="header-area">

                                    </div>
                                </div>
                                <!-- Contact Information -->
                                <div class="contact-info">
                                    <ul>
                                        <li><h3 class="fas fa-map-marker-alt"> 123 Ngũ Hành Sơn, TP Đà Nẵng</h3></li> <br>
                                        <li><h3 class="fas fa-phone"> +84 1900 8369</h3></li> <br>
                                        <li><h3 class="far fa-envelope"> antam@gmail.com</h3></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </footer>
        <!-- Scroll Up -->
        <div id="back-top" >
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div>

        <!-- JS here -->

        <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
        <!-- Jquery, Popper, Bootstrap -->
        <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap.min.js"></script>
        <!-- Jquery Mobile Menu -->
        <script src="./assets/js/jquery.slicknav.min.js"></script>

        <!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="./assets/js/owl.carousel.min.js"></script>
        <script src="./assets/js/slick.min.js"></script>
        <!-- One Page, Animated-HeadLin -->
        <script src="./assets/js/wow.min.js"></script>
        <script src="./assets/js/animated.headline.js"></script>
        <script src="./assets/js/jquery.magnific-popup.js"></script>

        <!-- Date Picker -->
        <script src="./assets/js/gijgo.min.js"></script>
        <!-- Nice-select, sticky -->
        <script src="./assets/js/jquery.nice-select.min.js"></script>
        <script src="./assets/js/jquery.sticky.js"></script>

        <!-- counter , waypoint,Hover Direction -->
        <script src="./assets/js/jquery.counterup.min.js"></script>
        <script src="./assets/js/waypoints.min.js"></script>
        <script src="./assets/js/jquery.countdown.min.js"></script>
        <script src="./assets/js/hover-direction-snake.min.js"></script>

        <!-- contact js -->
        <script src="./assets/js/contact.js"></script>
        <script src="./assets/js/jquery.form.js"></script>
        <script src="./assets/js/jquery.validate.min.js"></script>
        <script src="./assets/js/mail-script.js"></script>
        <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

        <!-- Jquery Plugins, main Jquery -->	
        <script src="./assets/js/plugins.js"></script>
        <script src="./assets/js/main.js"></script>

    </body>
</html>